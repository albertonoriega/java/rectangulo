
package examen;

public class Rectangulo {
    
    
    // ATRIBUTOS
    public String nombre;
    public double base;
    public double altura;

    // CONSTRUCTOR POR DEFECTO
    public Rectangulo() {
        this.nombre="";
        this.base=0;
        this.altura=0;
    }

    // CONTRUCTOR QUE RECIBE BASE Y ALTURA Y NOMBRE=VACIO
    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
        this.nombre="";
    }

    // CONSTRUCTOR QUE RECIBE LOS TRES DATOS
    public Rectangulo(String nombre, double base, double altura) {
        this.nombre = nombre;
        this.base = base;
        this.altura = altura;
    }
    
    // METODO QUE DEVUELVE EL AREA DEL RECTANGULO
    public double getArea() {
        return this.base * this.altura;
    }

    // METODO TOSTRING DE NOMBRE, AREA Y ALTURA
    public String toString() {
        return "Nombre: " + this.nombre + ", base: " + this.base + ", altura: " + altura;
    }
    
    // METODO QUE DEVUELVE UN BOOLEANO SI EL RECTANGULO ES CUADRADO
    public boolean isCuadrado() {
        if (this.base==this.altura){
            return true;
        } 
            return false;
        
    }

    // GETTERS Y SETTERS
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
        
}
