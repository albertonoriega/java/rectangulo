/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package examen;

/**
 *
 * @author alberto
 */
public class Examen {

    
    public static void main(String[] args) {
        
        // INSTANCIAR TRES OBJETOS
        Rectangulo rec1, rec2, rec3;
        
        // INICIALIZA rec1 CONSTRUCTOR POR DEFECTO
        rec1= new Rectangulo();
        
        // INICIALIZA rec2 BASE X ALURA 4 X 7 
        rec2= new Rectangulo(4, 7);
        
        // INICIALIZA rec3 BASE X ALURA 5 X 5 Y NOMBRE Rectángulo3 
        rec3= new Rectangulo("Rectángulo3", 5, 5);
        
        // ESTABLECE COMO ATRIBUTO NOMBRE DE rec1 Rectángulo1       
        rec1.setNombre("Rectángulo1");
        
        // ESTABLECE COMO ATRIBUTO NOMBRE DE rec2 Rectángulo2
        rec2.setNombre("Rectángulo2");
        
        // ESTABLECE LA ALTURA DE rec1 A 12 Y LA BASE A 15
        rec1.setAltura(12);
        
        rec1.setBase(15);
        
        // UTILIZA LOS METODOS toString Y isCuadrado CON LOS TRES OBJETOS CREADOS
        
        System.out.println("¿Es cuadrado?: "+ rec1.isCuadrado());
        
        System.out.println(rec1.toString());
        
        System.out.println("¿Es cuadrado?: " + rec2.isCuadrado());
        
        System.out.println(rec2.toString());
        
        System.out.println("¿Es cuadrado?: "+ rec3.isCuadrado());
        
        System.out.println(rec3.toString());
        
        
        
                
    }
    
}
